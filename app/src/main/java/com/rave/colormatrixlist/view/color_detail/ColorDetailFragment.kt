package com.rave.colormatrixlist.view.color_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.rave.colormatrixlist.adapter.ColorDetailAdapter
import com.rave.colormatrixlist.databinding.FragmentColorDetailBinding

class ColorDetailFragment : Fragment() {

    private var _binding: FragmentColorDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ColorDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val numList = args.color.toString().mapNotNull { it.digitToIntOrNull() }
        binding.rvColors.adapter = ColorDetailAdapter(numList)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}