package com.rave.colormatrixlist.view.colors

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rave.colormatrixlist.adapter.ColorAdapter
import com.rave.colormatrixlist.databinding.FragmentColorBinding
import com.rave.colormatrixlist.viewmodel.ColorViewModel

class ColorFragment : Fragment() {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()
    private val colorAdapter by lazy { ColorAdapter(::colorClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() = with(binding) {
        rvColors.adapter = colorAdapter
        btnFetch.setOnClickListener {
            etCount.text.toString().toIntOrNull()?.let { count ->
                colorViewModel.count = count
            }
        }
    }

    private fun initObservers() = with(colorViewModel) {
        state.observe(viewLifecycleOwner) { state ->
            binding.loading.isVisible = state.isLoading
            colorAdapter.addColors(state.colorList)
        }
    }

    private fun colorClicked(color: Int) {
        val colorDetailNav = ColorFragmentDirections.actionColorFragmentToColorDetailFragment(color)
        findNavController().navigate(colorDetailNav)
    }
}