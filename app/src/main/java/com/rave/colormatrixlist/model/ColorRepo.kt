package com.rave.colormatrixlist.model

import android.graphics.Color
import com.rave.colormatrixlist.model.remote.ColorService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.random.Random

object ColorRepo {

    private val colorService = object : ColorService {
        override suspend fun getColors(count: Int): List<Int> {
            return mutableListOf<Int>().apply {
                repeat(count) {
                    val color = Color.rgb(
                        Random.nextInt(255),
                        Random.nextInt(255),
                        Random.nextInt(255)
                    )
                    add(color)
                }
            }
        }
    }


    suspend fun getColors(count: Int) = withContext(Dispatchers.IO) {
        colorService.getColors(count)
    }
}