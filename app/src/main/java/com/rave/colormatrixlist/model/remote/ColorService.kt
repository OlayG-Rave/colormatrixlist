package com.rave.colormatrixlist.model.remote

interface ColorService {

    suspend fun getColors(count: Int) : List<Int>
}