package com.rave.colormatrixlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView
import com.rave.colormatrixlist.databinding.ItemColorBinding

class ColorAdapter(
    private val lksdfjl: (Int) -> Unit
) : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    @ColorInt
    private val colors: MutableList<Int> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ColorViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { lksdfjl(colors[adapterPosition]) }
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        holder.bindColor(colors[position])
    }

    override fun getItemCount() = colors.size

    fun addColors(@ColorInt colors: List<Int>) {
        this.colors.run {
            val oldCount = size
            clear()
            notifyItemRangeRemoved(0, oldCount)

            addAll(colors)
            notifyItemRangeInserted(0, colors.size)
        }
    }

    class ColorViewHolder(
        private val binding: ItemColorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindColor(@ColorInt color: Int) {
            binding.root.setBackgroundColor(color)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemColorBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ColorViewHolder(it) }
        }
    }
}