package com.rave.colormatrixlist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.colormatrixlist.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {

    private val repo = ColorRepo

    private val _state = MutableLiveData(ColorState())
    val state: LiveData<ColorState> get() = _state

    var count: Int = 0
        set(value) {
            field = value
            fetchColors(value)
        }

    init {
        fetchColors(20)
    }

    private fun fetchColors(count : Int) {
        viewModelScope.launch {
            val colors = repo.getColors(count)
            _state.value = ColorState(isLoading = false, colorList = colors)
        }
    }

}