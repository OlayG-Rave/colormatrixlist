package com.rave.colormatrixlist.viewmodel

import androidx.annotation.ColorInt

data class ColorState(
    val isLoading: Boolean = false,
    @ColorInt val colorList: List< Int> = emptyList()
)
